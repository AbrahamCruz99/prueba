package programacionmodular;

import java.util.Scanner;

public class Potencia {
	public static void main(String args[]) {
		int base = pedirBase();
		int expo = pedirExponente();
		int calc = calcularPotencia(base, expo);
		verResultado (base, expo, calc);
	}
	/////////////////////////////////////////////////////
	public static int pedirBase() {
		Scanner entrada = new Scanner (System.in);
		System.out.println("Introduce la base");
		return entrada.nextInt();
	}
	/////////////////////////////////////////////////////
	public static int pedirExponente() {
		Scanner entrada = new Scanner (System.in);
		System.out.println("Introduce el exponente");
		return entrada.nextInt();
	}
	////////////////////////////////////////////////////
	public static int calcularPotencia(int base, int expo) {
		int resultado = 1;
		for (int i = 0; i < expo; i++) {
			resultado = resultado * base;
		}
		return resultado;
	}
////////////////////////////////////////////////////
	public static void verResultado(int base, int expo, int calc) {
		System.out.println(base + " elevado a " + expo + " = " + calc);
	}
}
