/* Trabajo realizado con Alejandro Rodriguez*/

/*Primero clono el repositorio remoto de Alex con el comando git init sabiendo la direccion de enlace que el me facilita*/
ahm@ahm-VirtualBox:~/Documentos/Git/Repositorio1$ git clone https://13breaker@bitbucket.org/13breaker/trabajogrupo.git
Clonando en 'trabajogrupo'...
remote: Counting objects: 7, done.
remote: Compressing objects: 100% (7/7), done.
remote: Total 7 (delta 1), reused 0 (delta 0)
Desempaquetando objetos: 100% (7/7), listo.

/*Ahora inicializo el git para luego poder añadir el repositorio remoto a mi repositorio local*/
ahm@ahm-VirtualBox:~/Documentos/Git/Repositorio1$ git init
Reinicializado el repositorio Git existente en /home/ahm/Documentos/Git/Repositorio1/.git/
ahm@ahm-VirtualBox:~/Documentos/Git/Repositorio1$ git status
En la rama master
Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)
	trabajogrupo/

no hay nada agregado al commit pero hay archivos sin seguimiento presentes (usa "git add" para hacerles seguimiento)

/*Añado el repositorio remoto a mi repositorio local*/
ahm@ahm-VirtualBox:~/Documentos/Git/Repositorio1$ git add trabajogrupo/

/* Realizo dos cambios: borro un archivo .jpg y modifco el nombre del archivo "imagen.jpg" a "imagenmodificada2.jpg"*/
ahm@ahm-VirtualBox:~/Documentos/Git/Repositorio1$ git status
En la rama master
Cambios a ser confirmados:
  (usa "git restore --staged <archivo>..." para sacar del área de stage)
	nuevo archivo:  trabajogrupo/UT-3.A-CORREGIDO.pdf
	nuevo archivo:  trabajogrupo/UT3_SQL
	nuevo archivo:  trabajogrupo/imagenmodificada2.jpg

/*Confirmo que la carpeta "trabajogrupo" se convierta en una segunda version.*/
ahm@ahm-VirtualBox:~/Documentos/Git/Repositorio1$ git commit -m "Segunda version"
[master abe0951] Segunda version
 3 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 trabajogrupo/UT-3.A-CORREGIDO.pdf
 create mode 100644 trabajogrupo/UT3_SQL
 create mode 100644 trabajogrupo/imagenmodificada2.jpg

ahm@ahm-VirtualBox:~/Documentos/Git/Repositorio1$ git status
En la rama master
nada para hacer commit, el árbol de trabajo está limpio

/* Realizamos git push para que la carpeta "trabajogrupo" se guarde en mi repositorio remoto de bitbucket*/
ahm@ahm-VirtualBox:~/Documentos/Git/Repositorio1$ git push https://AbrahamCruz99@bitbucket.org/AbrahamCruz99/proyecto2.git master
Password for 'https://AbrahamCruz99@bitbucket.org': 
Enumerando objetos: 7, listo.
Contando objetos: 100% (7/7), listo.
Comprimiendo objetos: 100% (6/6), listo.
Escribiendo objetos: 100% (6/6), 2.62 MiB | 1.89 MiB/s, listo.
Total 6 (delta 0), reusado 0 (delta 0)
To https://bitbucket.org/AbrahamCruz99/proyecto2.git
   5036659..abe0951  master -> master
   
