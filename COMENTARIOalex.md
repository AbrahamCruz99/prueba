/*INICIALIZAMOS GIT*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git init
Reinicializado el repositorio Git existente en /home/aleeex/Entornos/Ficheros/.git/


/*COMPROBAMOS EL ESTADO*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git status
En la rama master

No hay commits todavía

Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)

	20191120_115858.jpg
	UT-3.A-CORREGIDO.pdf
	UT3_SQL

no hay nada agregado al commit pero hay archivos sin seguimiento presentes (usa "git add" para hacerles seguimiento)

/*AÑADIMOS LOS ARCHIVOS AL REPOSITORIO*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git add UT3_SQL
aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git add UT-3.A-CORREGIDO.pdf
aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git add 20191120_115858.jpg

/*VOLVEMOS A COMPROBAR EL ESTADO*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git status
En la rama master

No hay commits todavía

Cambios a ser confirmados:
  (usa "git rm --cached <archivo>..." para sacar del área de stage)

	nuevo archivo:  20191120_115858.jpg
	nuevo archivo:  UT-3.A-CORREGIDO.pdf
	nuevo archivo:  UT3_SQL

/*CONFIRMAMOS Y LO NOMBRAMOS COMO "subido"*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git commit -m "subido"
[master (commit-raíz) e45f758] subido
 3 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 20191120_115858.jpg
 create mode 100644 UT-3.A-CORREGIDO.pdf
 create mode 100644 UT3_SQL
 
/*VOLVEMOS A COMPROBAR EL ESTADO*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git status
En la rama master
nada para hacer commit, el árbol de trabajo está limpio

/*HACEMOS UN PUSH PARA LLEVARLO A BITBUCKET*/
aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git push https://13breaker@bitbucket.org/13breaker/trabajogrupo.git master
Password for 'https://13breaker@bitbucket.org': 
Enumerando objetos: 5, listo.
Contando objetos: 100% (5/5), listo.
Comprimiendo objetos: 100% (5/5), listo.
Escribiendo objetos: 100% (5/5), 2.62 MiB | 346.00 KiB/s, listo.
Total 5 (delta 0), reusado 0 (delta 0)
To https://bitbucket.org/13breaker/trabajogrupo.git
 * [new branch]      master -> master
 
 /*VOLVEMOS A COMPROBAR EL ESTADO (AQUI HEMOS CAMBIADO EL NOMBRE A LA IMAGEN)*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git status
En la rama master
Cambios no rastreados para el commit:
  (usa "git add/rm <archivo>..." para actualizar a lo que se le va a hacer commit)
  (usa "git checkout -- <archivo>..." para descartar los cambios en el directorio de trabajo)

	borrado:        20191120_115858.jpg

Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)

	imagen.jpg

sin cambios agregados al commit (usa "git add" y/o "git commit -a")

/*AÑADIMOS LA IMAGEN*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git add imagen.jpg

/*CONFIRMAMOS EL ARCHIVO*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git commit -m "nombrecambiado"
[master a4438cc] nombrecambiado
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 imagen.jpg

/*VOLVEMOS A COMPROBAR EL ESTADO*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git status
En la rama master
Cambios no rastreados para el commit:
  (usa "git add/rm <archivo>..." para actualizar a lo que se le va a hacer commit)
  (usa "git checkout -- <archivo>..." para descartar los cambios en el directorio de trabajo)

	borrado:        20191120_115858.jpg

sin cambios agregados al commit (usa "git add" y/o "git commit -a")

/*VOLVEMOS A SUBIRLO AL REPOSITORIO*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git push https://13breaker@bitbucket.org/13breaker/trabajogrupo.git master
Password for 'https://13breaker@bitbucket.org': 
Enumerando objetos: 3, listo.
Contando objetos: 100% (3/3), listo.
Comprimiendo objetos: 100% (2/2), listo.
Escribiendo objetos: 100% (2/2), 261 bytes | 261.00 KiB/s, listo.
Total 2 (delta 1), reusado 0 (delta 0)
To https://bitbucket.org/13breaker/trabajogrupo.git
   e45f758..a4438cc  master -> master
   
/*VOLVEMOS A COMPROBAR EL ESTADO*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git status
En la rama master
Cambios no rastreados para el commit:
  (usa "git add/rm <archivo>..." para actualizar a lo que se le va a hacer commit)
  (usa "git checkout -- <archivo>..." para descartar los cambios en el directorio de trabajo)

	borrado:        20191120_115858.jpg

sin cambios agregados al commit (usa "git add" y/o "git commit -a")


AHORA VAMOS A CLONAR EL ARCHIVO MODIFICADO POR ABRAHAM
-----------------------------------------------------------------------------------------------------------------------

/*HACEMOS UN CLONE*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git clone https://AbrahamCruz99@bitbucket.org/AbrahamCruz99/proyecto2.git 
Clonando en 'proyecto2'...
Password for 'https://AbrahamCruz99@bitbucket.org': 
remote: Counting objects: 136, done.
remote: Compressing objects: 100% (120/120), done.
remote: Total 136 (delta 15), reused 126 (delta 14)
Recibiendo objetos: 100% (136/136), 2.68 MiB | 380.00 KiB/s, listo.
Resolviendo deltas: 100% (15/15), listo.

/*VOLVEMOS A COMPROBAR EL ESTADO*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git status
En la rama master
Cambios no rastreados para el commit:
  (usa "git add/rm <archivo>..." para actualizar a lo que se le va a hacer commit)
  (usa "git checkout -- <archivo>..." para descartar los cambios en el directorio de trabajo)

	borrado:        20191120_115858.jpg

Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)

	proyecto2/

sin cambios agregados al commit (usa "git add" y/o "git commit -a")

/*AÑADIMOS EL PROYECTO CLONADO*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git add proyecto2/

/*VOLVEMOS A COMPROBAR EL ESTADO*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git status
En la rama master
Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	nuevo archivo:  proyecto2/Introduccion/.classpath
	nuevo archivo:  proyecto2/Introduccion/.project
	nuevo archivo:  proyecto2/Introduccion/.settings/org.eclipse.jdt.core.prefs
	nuevo archivo:  proyecto2/Introduccion/bin/AbrahamCruz/Ecuacion.class
	nuevo archivo:  proyecto2/Introduccion/bin/AbrahamCruz/Ejercicio1.class
	nuevo archivo:  proyecto2/Introduccion/bin/AbrahamCruz/Ejercicio3.class
	nuevo archivo:  proyecto2/Introduccion/bin/AbrahamCruz/NumeroPrimo.class
	nuevo archivo:  proyecto2/Introduccion/bin/POO/ClaseCaracter.class
	nuevo archivo:  proyecto2/Introduccion/bin/POO/Figura.class
	nuevo archivo:  proyecto2/Introduccion/bin/POO/claseString.class
	nuevo archivo:  proyecto2/Introduccion/bin/Pildoras/Coche.class
	nuevo archivo:  proyecto2/Introduccion/bin/Pildoras/ForCuentaAtras.class
	nuevo archivo:  proyecto2/Introduccion/bin/Pildoras/Uso_Coche.class
	nuevo archivo:  proyecto2/Introduccion/bin/Pildoras/factorial6.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/AreaRectangulo.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/Calculadora.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/CalculadoraMatias.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/ConjuntoNumeros.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/ControlD.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/ConversorNotaSwitch.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/CuentaPalabrasAlonso.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/CuentaPalabrasMiguelB.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/CuentaPalabrasMiguelG.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/DivisorCero.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/DoWhile.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/DoWhileVersionDos.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/Enter.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/ForAnidado.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/Impuestos.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/Llamada.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/Mcd.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/MediaNotas.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/NotasMaxMin.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/NumeroFactorial.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/NumeroMenor.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/NumeroPositivo.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/PalabrasMatias.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/Par.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/Potencia.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/SumaSerie.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/TablasMult2.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/TrianguloPitagorico.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/forAbecedario.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/tablaMultiplicar.class
	nuevo archivo:  proyecto2/Introduccion/bin/estructurasdecontrol/trianguloprueba.class
	nuevo archivo:  proyecto2/Introduccion/bin/programacionmodular/AreaRectangulo.class
	nuevo archivo:  proyecto2/Introduccion/bin/programacionmodular/CalculadoraModular.class
	nuevo archivo:  proyecto2/Introduccion/bin/programacionmodular/CambioBase.class
	nuevo archivo:  proyecto2/Introduccion/bin/programacionmodular/Factorial.class
	nuevo archivo:  proyecto2/Introduccion/bin/programacionmodular/FactorialRecursivo.class
	nuevo archivo:  proyecto2/Introduccion/bin/programacionmodular/Fecha.class
	nuevo archivo:  proyecto2/Introduccion/bin/programacionmodular/Mcd.class
	nuevo archivo:  proyecto2/Introduccion/bin/programacionmodular/McdRecursivo.class
	nuevo archivo:  proyecto2/Introduccion/bin/programacionmodular/Potencia.class
	nuevo archivo:  proyecto2/Introduccion/bin/programacionmodular/PotenciaRecursiva.class
	nuevo archivo:  proyecto2/Introduccion/bin/programacionmodular/Salario.class
	nuevo archivo:  proyecto2/Introduccion/bin/programacionmodular/combinaciones.class
	nuevo archivo:  proyecto2/Introduccion/src/AbrahamCruz/Ecuacion.java
	nuevo archivo:  proyecto2/Introduccion/src/AbrahamCruz/Ejercicio1.java
	nuevo archivo:  proyecto2/Introduccion/src/AbrahamCruz/Ejercicio3.java
	nuevo archivo:  proyecto2/Introduccion/src/AbrahamCruz/NumeroPrimo.java
	nuevo archivo:  proyecto2/Introduccion/src/POO/ClaseCaracter.java
	nuevo archivo:  proyecto2/Introduccion/src/POO/Figura.java
	nuevo archivo:  proyecto2/Introduccion/src/POO/claseString.java
	nuevo archivo:  proyecto2/Introduccion/src/Pildoras/Coche.java
	nuevo archivo:  proyecto2/Introduccion/src/Pildoras/ForCuentaAtras.java
	nuevo archivo:  proyecto2/Introduccion/src/Pildoras/Uso_Coche.java
	nuevo archivo:  proyecto2/Introduccion/src/Pildoras/factorial6.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/AreaRectangulo.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/Calculadora.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/CalculadoraMatias.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/ConjuntoNumeros.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/ControlD.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/ConversorNotaSwitch.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/CuentaPalabrasAlonso.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/CuentaPalabrasMiguelB.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/CuentaPalabrasMiguelG.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/DivisorCero.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/DoWhile.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/DoWhileVersionDos.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/Enter.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/ForAnidado.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/Impuestos.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/Llamada.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/Mcd.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/MediaNotas.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/NotasMaxMin.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/NumeroFactorial.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/NumeroMenor.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/NumeroPositivo.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/PalabrasMatias.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/Par.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/Potencia.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/SumaSerie.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/TablasMult2.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/TrianguloPitagorico.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/forAbecedario.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/tablaMultiplicar.java
	nuevo archivo:  proyecto2/Introduccion/src/estructurasdecontrol/trianguloprueba.java
	nuevo archivo:  proyecto2/Introduccion/src/programacionmodular/AreaRectangulo.java
	nuevo archivo:  proyecto2/Introduccion/src/programacionmodular/CalculadoraModular.java
	nuevo archivo:  proyecto2/Introduccion/src/programacionmodular/CambioBase.java
	nuevo archivo:  proyecto2/Introduccion/src/programacionmodular/Factorial.java
	nuevo archivo:  proyecto2/Introduccion/src/programacionmodular/FactorialRecursivo.java
	nuevo archivo:  proyecto2/Introduccion/src/programacionmodular/Fecha.java
	nuevo archivo:  proyecto2/Introduccion/src/programacionmodular/Mcd.java
	nuevo archivo:  proyecto2/Introduccion/src/programacionmodular/McdRecursivo.java
	nuevo archivo:  proyecto2/Introduccion/src/programacionmodular/Potencia.java
	nuevo archivo:  proyecto2/Introduccion/src/programacionmodular/PotenciaRecursiva.java
	nuevo archivo:  proyecto2/Introduccion/src/programacionmodular/Salario.java
	nuevo archivo:  proyecto2/Introduccion/src/programacionmodular/combinaciones.java
	nuevo archivo:  proyecto2/README.md
	nuevo archivo:  proyecto2/trabajogrupo/UT-3.CAMBIADO.pdf
	nuevo archivo:  proyecto2/trabajogrupo/UT3_SQL
	nuevo archivo:  proyecto2/trabajogrupo/imagenmodificada2.jpg

/*HACEMOS UN COMMMIT NOMBRADO "Version2"*/

aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git commit -m "Version2"
[master 3cc395e] Version2
 115 files changed, 2116 insertions(+)
 create mode 100644 proyecto2/Introduccion/.classpath
 create mode 100644 proyecto2/Introduccion/.project
 create mode 100644 proyecto2/Introduccion/.settings/org.eclipse.jdt.core.prefs
 create mode 100644 proyecto2/Introduccion/bin/AbrahamCruz/Ecuacion.class
 create mode 100644 proyecto2/Introduccion/bin/AbrahamCruz/Ejercicio1.class
 create mode 100644 proyecto2/Introduccion/bin/AbrahamCruz/Ejercicio3.class
 create mode 100644 proyecto2/Introduccion/bin/AbrahamCruz/NumeroPrimo.class
 create mode 100644 proyecto2/Introduccion/bin/POO/ClaseCaracter.class
 create mode 100644 proyecto2/Introduccion/bin/POO/Figura.class
 create mode 100644 proyecto2/Introduccion/bin/POO/claseString.class
 create mode 100644 proyecto2/Introduccion/bin/Pildoras/Coche.class
 create mode 100644 proyecto2/Introduccion/bin/Pildoras/ForCuentaAtras.class
 create mode 100644 proyecto2/Introduccion/bin/Pildoras/Uso_Coche.class
 create mode 100644 proyecto2/Introduccion/bin/Pildoras/factorial6.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/AreaRectangulo.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/Calculadora.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/CalculadoraMatias.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/ConjuntoNumeros.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/ControlD.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/ConversorNotaSwitch.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/CuentaPalabrasAlonso.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/CuentaPalabrasMiguelB.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/CuentaPalabrasMiguelG.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/DivisorCero.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/DoWhile.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/DoWhileVersionDos.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/Enter.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/ForAnidado.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/Impuestos.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/Llamada.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/Mcd.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/MediaNotas.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/NotasMaxMin.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/NumeroFactorial.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/NumeroMenor.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/NumeroPositivo.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/PalabrasMatias.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/Par.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/Potencia.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/SumaSerie.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/TablasMult2.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/TrianguloPitagorico.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/forAbecedario.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/tablaMultiplicar.class
 create mode 100644 proyecto2/Introduccion/bin/estructurasdecontrol/trianguloprueba.class
 create mode 100644 proyecto2/Introduccion/bin/programacionmodular/AreaRectangulo.class
 create mode 100644 proyecto2/Introduccion/bin/programacionmodular/CalculadoraModular.class
 create mode 100644 proyecto2/Introduccion/bin/programacionmodular/CambioBase.class
 create mode 100644 proyecto2/Introduccion/bin/programacionmodular/Factorial.class
 create mode 100644 proyecto2/Introduccion/bin/programacionmodular/FactorialRecursivo.class
 create mode 100644 proyecto2/Introduccion/bin/programacionmodular/Fecha.class
 create mode 100644 proyecto2/Introduccion/bin/programacionmodular/Mcd.class
 create mode 100644 proyecto2/Introduccion/bin/programacionmodular/McdRecursivo.class
 create mode 100644 proyecto2/Introduccion/bin/programacionmodular/Potencia.class
 create mode 100644 proyecto2/Introduccion/bin/programacionmodular/PotenciaRecursiva.class
 create mode 100644 proyecto2/Introduccion/bin/programacionmodular/Salario.class
 create mode 100644 proyecto2/Introduccion/bin/programacionmodular/combinaciones.class
 create mode 100644 proyecto2/Introduccion/src/AbrahamCruz/Ecuacion.java
 create mode 100644 proyecto2/Introduccion/src/AbrahamCruz/Ejercicio1.java
 create mode 100644 proyecto2/Introduccion/src/AbrahamCruz/Ejercicio3.java
 create mode 100644 proyecto2/Introduccion/src/AbrahamCruz/NumeroPrimo.java
 create mode 100644 proyecto2/Introduccion/src/POO/ClaseCaracter.java
 create mode 100644 proyecto2/Introduccion/src/POO/Figura.java
 create mode 100644 proyecto2/Introduccion/src/POO/claseString.java
 create mode 100644 proyecto2/Introduccion/src/Pildoras/Coche.java
 create mode 100644 proyecto2/Introduccion/src/Pildoras/ForCuentaAtras.java
 create mode 100644 proyecto2/Introduccion/src/Pildoras/Uso_Coche.java
 create mode 100644 proyecto2/Introduccion/src/Pildoras/factorial6.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/AreaRectangulo.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/Calculadora.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/CalculadoraMatias.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/ConjuntoNumeros.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/ControlD.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/ConversorNotaSwitch.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/CuentaPalabrasAlonso.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/CuentaPalabrasMiguelB.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/CuentaPalabrasMiguelG.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/DivisorCero.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/DoWhile.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/DoWhileVersionDos.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/Enter.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/ForAnidado.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/Impuestos.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/Llamada.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/Mcd.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/MediaNotas.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/NotasMaxMin.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/NumeroFactorial.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/NumeroMenor.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/NumeroPositivo.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/PalabrasMatias.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/Par.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/Potencia.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/SumaSerie.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/TablasMult2.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/TrianguloPitagorico.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/forAbecedario.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/tablaMultiplicar.java
 create mode 100644 proyecto2/Introduccion/src/estructurasdecontrol/trianguloprueba.java
 create mode 100644 proyecto2/Introduccion/src/programacionmodular/AreaRectangulo.java
 create mode 100644 proyecto2/Introduccion/src/programacionmodular/CalculadoraModular.java
 create mode 100644 proyecto2/Introduccion/src/programacionmodular/CambioBase.java
 create mode 100644 proyecto2/Introduccion/src/programacionmodular/Factorial.java
 create mode 100644 proyecto2/Introduccion/src/programacionmodular/FactorialRecursivo.java
 create mode 100644 proyecto2/Introduccion/src/programacionmodular/Fecha.java
 create mode 100644 proyecto2/Introduccion/src/programacionmodular/Mcd.java
 create mode 100644 proyecto2/Introduccion/src/programacionmodular/McdRecursivo.java
 create mode 100644 proyecto2/Introduccion/src/programacionmodular/Potencia.java
 create mode 100644 proyecto2/Introduccion/src/programacionmodular/PotenciaRecursiva.java
 create mode 100644 proyecto2/Introduccion/src/programacionmodular/Salario.java
 create mode 100644 proyecto2/Introduccion/src/programacionmodular/combinaciones.java
 create mode 100644 proyecto2/README.md
 create mode 100644 proyecto2/trabajogrupo/UT-3.CAMBIADO.pdf
 create mode 100644 proyecto2/trabajogrupo/UT3_SQL
 create mode 100644 proyecto2/trabajogrupo/imagenmodificada2.jpg
 
/*HACEMOS UN PUSH Y LISTO*/
aleeex@aleeex-VirtualBox:~/Entornos/Ficheros$ git push https://13breaker@bitbucket.org/13breaker/trabajogrupo.git master
Password for 'https://13breaker@bitbucket.org': 
Enumerando objetos: 131, listo.
Contando objetos: 100% (131/131), listo.
Comprimiendo objetos: 100% (129/129), listo.
Escribiendo objetos: 100% (130/130), 63.01 KiB | 1.80 MiB/s, listo.
Total 130 (delta 15), reusado 0 (delta 0)
remote: Resolving deltas: 100% (15/15), completed with 1 local object.
To https://bitbucket.org/13breaker/trabajogrupo.git
   a4438cc..3cc395e  master -> master
